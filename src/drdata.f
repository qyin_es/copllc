c     file  drdata fortran
c
      subroutine drdata(m,n, ia,ja,an, b,c, ih,jh,hn, obj, core,flag)

c     Purpose     :  Data handling
c     Date        :  May , 97

      real*8  core(1), obj
      integer ratio, lsp
      common  /SPA/ ratio, lsp
      integer STDIN, STDOUT
      common  /IO/ STDIN, STDOUT

      integer flag

      integer m,n, ia,ja,an, b,c, ih,jh,hn
c     integer nhgp

      ispace = 0

      flag  = 0
      obj   = 0.d0
      lcore = lsp

c     readin data
c     m,n,na,nh,opt,c,b,ia,ja,an,ih,jh,hn
      call datain(m,n,na,nh,ia,ja,an, b,c, ih,jh,hn,
     &  obj,core,lcore,iflag)

      if (iflag .lt. 0) then
         if (iflag .eq. -1) then
	     flag = 6
         else
	     flag = 7
         endif
	 call myexit(flag)
	 return
      endif

c GP
c      write(*,*) ' degree of diffic', n-m

         if (ispace.gt.0) then
         write(*,*) ' matrix H .......', n, nh
         endif

      return
      end

c     file  datain fortran
c
      subroutine datain(m,n,nz,nh,ia,ja,an, b,c, ih,jh,hn,
     &  opt,core,lcore,iflag)

C     Purpose     :  Readin data: A,b,c 
C     Date        :  May, 97
C     By          :  Yinyu Ye (Un. of Iowa)

      parameter (MaxColumns=20000, MaxRows=5000)
c
c     ***************************************
c      for solving GP:
c      common /GP/ IP, NCUM, CLG, SUML
c      IP -- number constraints in primal GP.
c      NCUM -- number of terms in the obj and each constraint 
c      CLG --  logarithm of the coefficient of each posynomial term
c      SUML --- sum of the dual variables for each constraint
c      real*8  CLG(MaxColumns), SUML(MaxRows)
c      integer IP, NCUM(MaxRows)
c     ***************************************
c
      integer ratio, lsp
      common  /SPA/ ratio, lsp
      real*8  core(1), opt

      integer idat,m,n,nz,nh,ia,ja,an, b,c, ih,jh,hn,lcore,iflag

      common  /IO/sdtin,sdtout
      integer sdtin,sdtout

      integer      iosgen
      character*11 chform
      character*7  chstat
      character*12 filstr

c     structure
c       m -# of constraints, 
c       n -# of variables, 
c       nz-# of nonzero elements in A, 
c       nh-# of diagonal elements plus nonzeros in the 
c            up-half of the symmetric Hassian matrix H.
c       point to:
c       b - the right hand vector
c       c - the gradient vector
c       ia- the m dimension list shows # of nonzeros in each row of A
c       ja- the nz dimension list shows the column index of each nonzero
c           in A, ordered from left to right and row by row.
c       an- the nz dimension list has the numerical values of nonzeros in
c           A corresponding to list ja
c       ih- the n dimension list shows 1+# of nonzeros in each row of 
c           the up-half of H
c       jh- the nz dimension list shows the column index of the diagonal
c           and each nonzero in the up-half of H, 
c           ordered from left to right and row by row
c       hn- the nz dimension list has the numerical values of nonzeros in
c           the up-half of H corresponding to list jh.
c       iat,jat,ant: same for A'
      
      ierr = sdtout
c###  chstat = 'unknown'
      chstat = 'old'
      chform = 'formatted'
      iosgen = 1
      idah = 40

      idat   = idah
      filstr = 'test.dat'
      open(unit=idat,file=filstr,status=chstat,
     1     iostat=iosgen, err=998)

c     reset data

 1001 format(i8)
 1002 format(e24.8)
c     idah : ft40
      read(idah,*,err=998)
      read(idah,*,err=998) m,n,nz,nh

      if (n.ge.MaxColumns) then
          write(*,*) ' xxx',n,' terms in data file (>',MaxColumns,')'
          write(*,*) 'dimension of array CLG should be increased'
      endif
      if (m.ge.MaxRows) then
          write(*,*) ' xxx',m,' rows in data file (>',MaxRows,')'
          write(*,*)
     x    'dimension of arrays SUML and NCUM should be increased'
      endif

      if (n.ge.MaxColumns .or. m.ge.MaxRows) then
	 write(*,*) ' xxx ask author for advanced version '
         iflag = -1
         go to 999
      endif
         
      opt = 0.d0

      mr = (m-1)/ratio +1
      m1r= m/ratio +1
      nr = (n-1)/ratio +1
      n1r= n/ratio +1
      nzr= (nz-1)/ratio +1
      nhr= (nh-1)/ratio + 1
      nmr= (n+m-1)/ratio +1

      ihead = 1

      c     = ihead
      b     = c + n
      ia    = b + m 
      ja    = ia + m1r
      an    = ja + nzr
      ih    = an + nz 
      jh    = ih  + n1r
      hn    = jh  + nhr
      ihead = hn  + nh

      lsp   = lsp - ihead + 1
c      iat   = ih
c      jat   = iat + n1r
c      ant   = jat + nzr
c      ih1   = ant + nz
c      ihead = hn  + nh
c
c
      if (lsp .lt. 0) then
	 write(*,*) ' xxx insufficient space for readin'
	 iflag = 6
	 call myexit(iflag)
	 return
      endif

c GP
      write(*,*) '                  The LCCP model                    '
      write(*,*) ' ---------------------------------------------------'
      write(*,*)
      write(*,*) '                 variables  :  ', n
      write(*,*) '                 constraints:  ', m
      write(*,*) '               nonzeros in A:  ', nz
      write(*,*) '               nonzeros in H:  ', nh

c     set c, b, ia,ja,an
      iflag = 0
      call read_abc(idah,m,n,nz,core(c),core(b),
     1     core(ia),core(ja),core(an), iflag)

      if (iflag .lt. 0) then
	 iflag = -3
	 go to 999
      endif

c     set ih,jh,hn
      iflag = 0
      call readh(idah,n,nh,core(ih),core(jh),core(hn), iflag)

      if (iflag .lt. 0) then
	 iflag = -3
	 go to 999
      endif

      close(unit=idah)
      return

  998 iflag = -2
  999 write(*,*) ' xxx readin error'
      close(unit=idat)
      return

      end

      subroutine read_abc(idah,m,n,nz,c,b,ia,ja,an, flag)

      real*8  c(1),b(1), an(1)
      integer idah,n,m,nz,ia(1),ja(1), flag

      parameter (MaxColumns=20000, MaxRows=5000)
      common /LCCP/cc
      real*8  cc(MaxColumns)

      read(idah,*,err=999)
      read(idah,1002,err=999) (cc(i), i=1, n) 

      read(idah,*,err=999)
      read(idah,1002,err=999) (b(i), i=1, m) 

      do j = 1, n
	 c(j) = cc(j)
      end do

c     elements-in-row-i's starting position
      read(idah,*,err=999)
      read(idah,1001,err=999) (ia(i), i=1, m) 
      k = 1
      do i = 1, m
	 len   = ia(i)
	 ia(i)= k
	 k     = k + len
      end do
      ia(m+1) = k

c     each element's column index and its real value
      read(idah,*,err=999)
c      read(idah,1001,err=999) (ja(i), i=1, nz)
      read(idah,*,err=999) (ja(i), an(i), i=1, nz)

      return
  999 flag = -1
      return

 1001 format(i8)
 1002 format(e24.8)
c 1003 format(i8, e24.8)
      end

      subroutine readh(idah,n,nh,ih,jh,hn, flag)

      real*8  hn(1)
      integer idah,n,nh,ih(1),jh(1), flag

      k = 1
c
      if (nh .gt. 0) then	 
c        elements-in-row-i's starting position
        read(idah,*,err=999)
        read(idah,1001,err=999) (ih(i), i=1, n) 
        do i = 1, n
       	 len   = ih(i)
	 ih(i)= k
	 k     = k + len
       end do
       ih(n+1) = k

c       each element's column index and its real value
       read(idah,*,err=999)
c       read(idah,1001,err=999) (jh(i), i=1, nh)
       read(idah,*,err=999) (jh(i), hn(i), i=1, nh)
      else
        do i = 1, n+1
         ih(i) = k
        end do
      endif
      return
  999 flag = -1
      return

 1001 format(i8)
c 1003 format(i8, e24.8)
      end

