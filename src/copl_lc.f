c The Universal Permissive License (UPL), Version 1.0
c Subject to the condition set forth below, permission is hereby 
c granted to any person obtaining a copy of this software, 
c associated documentation and/or data (collectively the c 
c "Software"), free of charge and under any and all copyright 
c rights in the Software, and any and all patent rights owned or 
c freely licensable by each licensor hereunder covering either 
c (i) the unmodified Software as contributed to or provided by 
c such licensor, or (ii) the Larger Works (as defined below), to 
c deal in both
c
c (a) the Software, and
c
c (b) any piece of software and/or hardware listed in the 
c lrgrwrks.txt file if one is included with the Software (each a 
c �Larger Work� to which the Software is contributed by such 
c licensors), without restriction, including without limitation 
c the rights to copy, create derivative works of, display, 
c perform, and distribute the Software and make, use, sell, 
c offer for sale, import, export, have made, and have sold the 
c Software and the Larger Work(s), and to sublicense the 
c foregoing rights on either these or other terms.
c
c This license is subject to the following condition:
c
c The above copyright notice and either this complete permission 
c notice or at a minimum a reference to the UPL must be included 
c in all copies or substantial portions of the Software.
c
c THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY 
c KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE 
c WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
c PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
c COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
c LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
c OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
c SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
c
c     *****************************************************
c     *                                                   *
c     *                  COPL_LC                          *
c     *                                                   *
c     *a Linearly Constrained Convex Programming Optimizer*
c     *                                                   *
c     *         Computational Optimization Lab            *
c     *                University of Iowa                 *
c     *                                                   *
c     *         Version 1.1    June, 1997                 *
c     *                                                   *
c     *****************************************************
c
c     the main program follows.
c     it just provides the necessary core.

c     file  copl_lc fortran
c
      program copl_lc

      integer lensp
      parameter (lensp=4 000 000) 
      real*8  core(lensp)
      integer icore(1)
      equivalence (icore(1), core(1))

      call copl_lc0(core, lensp)

      stop
      end

