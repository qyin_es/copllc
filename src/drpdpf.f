c     file  drpdpf fortran
c
      subroutine drpdpf(m,n, ia,ja,an, b,c, ih,jh,hn, obj,
     1                  x,y,z, core, istop)

c     Purpose     :  DRive Primal-Dual Path-Following algorithm

      integer lensp, ratio, lsp
      common  /SPA/ ratio, lsp

C             for zero or negative pivot, set IZP just afer  JU
      INTEGER IZP,NZP
      COMMON  /PIVOT/IZP,NZP

      integer idiag, iha, ihb

      integer ia(1),ja(1), ih(1),jh(1)
      real*8  an(1),b(1),c(1),hn(1), x(1),y(1),z(1), core(1),obj

      integer iat,jat,ant, xr,xc,ixrc1,ixrc2, sign, p,ip,
     1        iua,iu,iju,ju, im,jm,ju1
      integer un,dn, d_x,d_y,d_z, r_P,r_D,r_mu, r,solu, xrc,ixrc

      ichksp= 0
      ispace= 0

      na    = ia(m+1)-1
      nh    = ih(n+1)-1
      mpn   = m + n

c+++  nm    = nh + na + m

c     nh may be lack of diagonal entries
	 idiag = 0
	 do j = 1, n
	    iha = ih(j)
	    ihb = ih(j+1) -1
	    if (ihb.lt.iha) then
	       idiag = idiag + 1
	    elseif(jh(iha).ne.j) then
c              H is ordered. Otherwise diagonal may not be the first entry.
c              At least, diagonal shoulb be the first entry, if any.
	       idiag = idiag + 1
	    endif
	 end do
      nm    = nh + idiag + na + m

      mr    = (m-1)/ratio + 1
      nr    = (n-1)/ratio + 1
      mpnr  = (mpn-1)/ratio + 1
      nar   = (na-1)/ratio + 1
      nmr   = (nm-1)/ratio + 1

      m1r   = m/ratio + 1
      n1r   = n/ratio + 1
      mpn1r = mpn/ratio + 1

      ihead = 1
		if (ichksp.eq.1) then
		   write(*,*) lsp
		   write(*,*) m,n, mpn, na, nh, nm
		   write(*,*) mr,nr, mpnr, nar, nhr, nmr
		   write(*,*) m1r,n1r, mpn1r
		endif
c+    iat,jat,ant, xr,xc,ixrc1,ixrc2, sign, p,ip, iua,iu,iju,ju, im,jm,ju1

      iat   = ihead
      jat   = iat + n1r
      ant   = jat + nar
      ihead = ant + na

      xr    = ihead
      xc    = xr  + m
      ixrc1 = ihead
      ixrc2 = ixrc1 + mpnr
      ihead = max(ixrc2 + mpnr, xc + n)

      sign  = ihead
      ihead = sign+ mpn

      p     = ihead
      ip    = p   + mpnr
      ihead = ip  + mpnr

c+    iua can be only nr
      iua   = ihead
      iu    = iua + mpnr
      iju   = iu  + mpn1r
      ju    = iju + mpnr

      im    = ju
      jm    = im  + mpn1r
      ju1   = jm  + nmr

		if (ichksp.eq.1) then
		   write(*,*) m,n, mpn, na, nh, nm
		   write(*,*) iat,jat,ant, xr,xc,ixrc1,ixrc2
		   write(*,*) sign, p,ip, iua,iu,iju,ju
		   write(*,*) im,jm,ju1
		endif

      call TGSM(m,n,ia,ja,an,core(iat),core(jat),core(ant))
c                  write(*,*) m,n, mpn, na, nh, nm
      
c     lcore are the total space left.
      lcore = (lsp - ju1 + 1)*ratio
      if (lcore .lt. 0) then
	 write(*,*) ' xxx insufficient space for sym_M'
	 istop = 6
	 call myexit(istop)
	 return
      endif

      call sym_M(m,n, ih,jh, core(iat),core(jat),
     1     core(iua),core(iu),core(iju),core(ju1),
     1     core(im),core(jm), lcore,nju,nu,
     2     core(sign), core(ixrc1),core(ixrc2),core(p),core(ip),
     2     iflag)
      if (iflag .ne. 0) then
	 write(*,*) ' xxx error in sym_M'
	 istop = 6
	 call myexit(istop)
      endif

         if (ispace.gt.0) then
         write(*,*) ' matrix A .......', m,n, na
         write(*,*) ' matrix U .......', mpn, nu
         endif

      call COPYI(nju,core(ju1),core(ju))

      njur  = (nju-1)/ratio + 1

      ihead = ju   + njur
C     zero pivot
      IZP   = ihead
      ihead = IZP  + mpnr
C     do not use IZP, just store after JU
      IZP   = nju

      dn    = ihead
      un    = dn   + mpn
      ihead = un   + nu

c+    un,dn, d_x,d_y,d_z, r_P,r_D,r_mu, r,solu, xrc,ixrc

      d_x   = ihead
      d_y   = d_x  + n
      d_z   = d_y  + m
      ihead = d_z  + n

      r_P   = ihead
      r_D   = r_P  + m
      r_mu  = r_D  + n
      ihead = r_mu + n

      r     = ihead
      solu  = r    + mpn
      ihead = solu + mpn

      xrc    = ihead
      ixrc   = xrc + mpn
      ihead  = ixrc+ mpnr

		if (ichksp.eq.1) then 
		   write(*,*) m,n, mpn, na, nh, nm
		   write(*,*) un,dn, d_x,d_y,d_z
		   write(*,*) r_P,r_D,r_mu, r,solu, xrc,ixrc
		endif

      lsp = lsp - ihead + 1

         if (ispace.gt.0) then
         write(*,*) ' remaining space ', lsp
         write(*,*) 
         write(*,*) ' m,n,mpn,na,nh,nm,nu,nju:', m,n,mpn,na,nh,nm,nu,nju
         endif

      write(*,*) 
      write(*,*) '             COPL_LC Running Progress               '
      write(*,*) ' -------------------------------------------------- '

      if (lsp .lt. 0) then
         write(*,*) 
	 write(*,*) ' xxx insufficient space for driving pdpfcp'
	 istop = 6
	 call myexit(istop)
	 return
      endif

      call pdpfcp(m,n, ia,ja,an, b,c, ih,jh,hn, obj, x,y,z, istop,
     1     core(iat),core(jat),core(ant),
     1     core(iua),core(iu),core(iju),core(ju),core(dn),core(un),
     2     core(r_P),core(r_D),core(r_mu),core(r),core(solu),
     2     core(sign),core(d_x),core(d_y),core(d_z),
     3     core(xr),core(xc),core(xrc),core(ixrc),core(p),core(ip))

      return
c     end of drpdpf()
      end
c     file  sym_m fortran
c     
      subroutine sym_M(m,n, ih,jh, iat,jat,
     1                 iua,iu,iju,ju, im,jm, lcore, nju,nu,
     2                 sign, ixrc1,ixrc2, p,ip, flag)

c     Purpose     :  perform symbolic operations
c     Last mod.   :  

      integer ih(1),jh(1)
      integer iat(1),jat(1), im(1),jm(1)
      integer iua(1), iu(1), iju(1),ju(1)
      integer p(1),ip(1)
      real*8  sign(1)

      integer ixrc1(1),ixrc2(1)
      integer m,n,mpn,km,k,i,j
      integer jumax, flag

c     input
c             m,n
c             lcore         space available
c             iat,jat
c             ih,jh
c     output
c             iua,iu,iju,ju
c             p, ip
c             sign
c     working
c             mpn,km,k,i,j
c             im,jm
c             ixrc1, ixrc2
c             jumax, flag

c        create structure of M (note: all diagonals are located)
	 km = 1
	 do 30 j = 1, n
	    im(j) = km
c           diagonal
	    iha = ih(j)
	    ihb = ih(j+1)-1

	    if (ihb.ge.iha .and. jh(iha).eq.j) then
c              H is ordered. Otherwise diagonal may not be the first entry.
c              At least, diagonal shoulb be the first entry, if any.
	       iha = iha + 1
c+             mn(km) = - hn(k)
c+          else
c+             mn(km) = 0.d0
	    endif
	    jm(km) = j
	    km     = km + 1

	    do 10 k = iha, ihb
	       jm(km) = jh(k)
c+             mn(km) = - hn(k)
	       km     = km + 1
   10       continue

c+          ima points the starting position of A(t)
c+          ima(j) = km
	    do 20 k = iat(j), iat(j+1)-1
	       jm(km) = jat(k) + n
c+             mn(km) = ant(k)
	       km     = km + 1
   20       continue
   30    continue

c        all diagonals are located
	 mpn = m+n
	 do 40 i =n+1, mpn
	    im(i)  = km
	    jm(km) = i
c+          mn(km) = 0.d0
	    km     = km + 1
   40    continue
	 im(mpn+1) = km

c        call order()
c        use the order:  (dx, dy)

c        set permutation
	 do 50 i =1, mpn
	    p(i) = i
	    ip(i)= i
   50    continue

c        set sign (they are related to p)
	 do 60 j =1, n
	    sign(j) = -1.
   60    continue
	 do 70 i=n+1, mpn
	    sign(i) = 1.
   70    continue

c        perform symbolic factorization to M
c+       call xint_alloc(Q,mpn)
c+       call xint_alloc(JL,mpn)

	 jumax = lcore
	 call SSF(n,mpn,p,ip,im,jm,iju,ju,iua,iu,
     1            jumax,ixrc1,ixrc2,flag,nju,nu)

      return

c     end of sym_M()
      end
c     file  pdpfcp fortran
c
      subroutine pdpfcp(m,n,ia,ja,an,b,c,ih,jh,hn,obj,x,y,z,istop,
     1                  iat,jat,ant, iua,iu,iju,ju,dn,un,
     2                  r_P,r_D,r_mu,r,solu,sign, d_x,d_y,d_z,
     3                  xr,xc, xrc, ixrc, p,ip)

c     Purpose     :  Primal-Dual Path-Following algorithm for
c                    Convex Programming with linear constraints

      real*8  an(1),b(1),c(1), hn(1), obj, obj_P, obj_D, DPFV, fun
      integer ia(1),ja(1), ih(1),jh(1)

      real*8  x(1),y(1),z(1)

      integer iter, itlm
      parameter (itlm = 100)
      common  /IT/ iter

      real*8  r_P(1), r_D(1)
      real*8  normrP, normrD, normx, normz, comp
      real*8  infrP, infrD, rcomp
      real*8  min_xz, max_xz
      real*8  big, small
      parameter (big = 1.d20, small = 1.d-15)

      real*8  tolx, tolz
      parameter (tolx = 1.d-14, tolz = 1.d-14)
      integer numx, numz

      real*8  epsP, epsD, epsC
      parameter (epsP = 1.d-8, epsD = 1.d-8, epsC = 1.d-12)
      logical feasP, feasD, reset
      integer istop

      integer iat(1), jat(1)
      real*8  ant(1)

      real*8  tolpiv
      parameter (tolpiv = 1.d-14)
      real*8  d_x(1),d_y(1),d_z(1)
      integer mpn, iua(1),iu(1),iju(1),ju(1), p(1),ip(1), flag
      real*8  dn(1), un(1), r_mu(1), r(1), solu(1)
      real*8  sign(1)

      real*8  gamma, eta,zeta, mu, mu_a

      real*8  alpha, alf_x, alf_z
      real*8  sigma, beta

      real*8  frac, dxdz
      real*8  alpha_R, alpha_N, alpha_L
      parameter (frac = 0.9995)
      real*8  alphaP, alphaD
      real*8  normdx, normdy

      real*8  norm_c, max_c, min_c
      integer iprint, icheck
      parameter (iprint =1, icheck = 1)

c     working array
      integer ixrc(1)
      real*8  xyz, xc(1), xr(1), xrc(1)
      real*8  zj, normrD_a, normrD_ah
      real*8  c0(1000)

      call init(m,n,x,y,z)
      mpn = m + n
      reset = .true.
      feasP = .false.
      feasD = .false.
      istop = -1

c     begin of main loop
      do 500 iter = 1, itlm

c-0-     check stopping criteria
c-1-     set local KKT system
	 
c        obj <-- Function f(x)
c        c   <-- Gradient f(x)
c        H   <-- Hessian f(x)

      call grad(n,c,ih,jh,hn,x)
	    norm_c = 0.d0
	    max_c  = 0.d0
	    min_c  = big
	    do j= 1, n
	       xyz = abs(c(j))
	       norm_c = norm_c + xyz*xyz
	       max_c  = max(max_c, xyz)
	       min_c  = min(min_c, xyz)
	    end do
	    call hessian(n,ih,jh,hn,x)
	    obj_P = fun(n,c,ih,jh,hn,x)

c           Lagrangian dual (for convex problem)
c           f(x) - gradient(t) x + b(t) y
	    obj_D = obj_P - DPFV(n,c,x) + DPFV(m,b,y)

	 numx   = 0
	 numz   = 0
	 normrP = 0.d0
	 normrD = 0.d0
	 normx  = 0.d0
	 normz  = 0.d0
	 comp   = 0.d0
	 min_xz = big
	 max_xz = 0.d0

	 call PSMFV(m,ia,ja,an,x,xr)
	 call PFVSM(m,n,ia,ja,an,y,xc)
	 do 130 i =1, m
	    r_P(i) = b(i) - xr(i)
	    normrP = normrP + abs(r_P(i))
  130    continue
	 do 140 j =1, n
	    if ( x(j) .lt. tolx ) numx = numx + 1
	    if ( z(j) .lt. tolz ) numz = numz + 1

	    if (reset) then
c           reset z(j) in terms of the sign and size of r_D(j) 
c              according to the following rules:
c              a. still not far away from the path
c              b. reduce the dual residuals 
	       xyz    = c(j) - xc(j)
	       zj     = z(j)
	       if (xyz.gt.0.d0) then
		  z(j)   = max(zj*1.d-2, xyz)
		  z(j)   = min(zj*1.d2, z(j))
	       elseif (xyz.lt.0.d0) then
c-                do nothing
c-                z(j)   = min(z(j), xyz)
	       endif
c           end of reset
	    endif
	    r_D(j) = c(j) - z(j) - xc(j)

	    normrD = normrD + abs(r_D(j))
	    normx  = normx  + abs(x(j))
	    normz  = normz  + abs(z(j))
	    xyz    = x(j)*z(j)
	    min_xz = min( min_xz, xyz)
	    max_xz = max( max_xz, xyz)
	    comp   = comp   + xyz
  140    continue
	 infrP  = normrP / ( 1.d0 + normx )
	 infrD  = normrD / ( 1.d0 + normz )
	 rcomp  = comp   / ( 1.d0 + normx + normz )
	 mu     = comp   / real(n)

	 sigma  = min_xz / mu
	 beta   = 0.d0
	 do 150 j =1, n
	    beta= beta + (x(j)*z(j) - mu)**2
  150    continue
	 beta   = sqrt(beta) / mu

	 write(*,4010) iter-1, mu, obj + obj_P, obj + obj_D, infrP, infrD

	 if(iprint.gt.1) then
	 write(*,*)
	 if (numx.gt.0) write(*,*) ' xxx   number of x < tolx:', numx
	 if (numz.gt.0) write(*,*) ' xxx   number of z < tolz:', numz
	 write(*,5010) 'c: norm,min,max', norm_c, min_c, max_c
	 write(*,5010) 'xz:  mu,min,max', mu, min_xz, max_xz
	 write(*,5010) 'norm: rP,rD,x,z', normrP, normrD, normx, normz
	 write(*,5010) 'rcom,sigma,beta', rcomp,sigma,beta, comp
	 endif

	 if ( infrP .lt. epsP ) then
	    feasP = .true.
	 else
	    feasP = .false.
	 endif

	 if ( infrD .lt. epsD ) then
	    feasD = .true.
	 else
	    feasD = .false.
	 endif

	 if (feasP .and. feasD ) then
	    if ( rcomp .lt. epsC ) then
	       istop = 0
c              write(*,*) 'exit -- optimal solution found'
	       go to 900
c              return
	    endif
	 endif

c-2-     solve for direction

c  a.    set       /  -1          T \
c             M =  |-X  Z - H    A  |
c                  \    A           /
c        note: M needs to be reset totally.

c        fill static part in M
	 call set_M(m,n, iat,jat,ant, iua,iu,iju,ju,dn,un, xr)

c        fill Hessian in M (note: it is P dependent)
	 do 205 j = 1, n
	    dn(j) = 0.d0
  205    continue
	 do 220 j = 1, n
	    do 210 k = ih(j), ih(j+1)-1
	       dn(jh(k)) = - hn(k)
  210       continue
c           iua points the starting position of A(t)
	    mua = iju(j) - iu(j)
	    do 215 km = iu(j), iua(j)-1
	       i = ju(mua+km)
	       un(km) = dn(i)
	       dn(i)  = 0.d0
  215       continue
  220    continue
	 do 225 j = 1, n
	    dn(j) = dn(j) - z(j)/x(j)
  225    continue

c  b.    factorize M
c+++     call PUSH(mpn,dn,iju,ju,iu,un, xrc,ixrc, tolpiv,flag,sign)
c        use r temp.
	 call PULL(mpn,dn,iju,ju,iu,un, ixrc, r, xrc, tolpiv,flag,sign)
	 if (flag .lt. 0) then
	    istop = 4
	    write(*,*) 'exit -- numerical difficulty'
	    write(*,*) '     -- flag =', flag
	    go to 900
c           call myexit(istop)
c           return
	 endif

c    ----predictor-corrector starts from here ---------------- 
       idir = 1
  888  continue
c  c.    set  r =  |zeta r_D - X(-1) r_mu |
c                  | eta r_P              |

       if(idir .eq. 0) then 
	 if (sigma .gt. 0.005 .and. beta .lt. 500. ) then
	    gamma = 0.3* min(0.1, sqrt(mu))
	 elseif (sigma .gt. 0.0005 .and. beta .lt. 1000. ) then
	    gamma = min(0.1, sqrt(mu))
	 elseif (sigma .gt. 0.00001 .and. beta .lt. 5000. ) then
	    gamma = 3.d0 * min(0.1, sqrt(mu))
	 else
	    gamma = 1.d0
	 endif
       elseif (idir .eq. 1) then 
	 gamma = 0.d0
       elseif (idir .eq. 2) then
	 gamma = 0.5 * mu_a/mu
       else
	 write(*,*) ' => input gamma : '
	 read(*,*) gamma
       endif
	 eta  = 1.d0 - gamma
	 zeta = 1.d0 - gamma
	 if(iprint.gt.1) then
	 write(*,*)
	 write(*,5010) 'gamma,eta,zeta ',gamma, eta,zeta, mu
	 endif
	 xyz = gamma*mu
	 do 230 j =1, n
	    r_mu(j) = xyz - x(j)*z(j)
	    r(j)    = zeta * r_D(j) - r_mu(j) / x(j)
  230    continue
	 do 240 i =1, m
	    r(i+n) = eta * r_P(i)
  240    continue

c  d.    forward and backward substitution
	 call SNS(mpn, p, dn, iju, ju, iu, un, solu, r, xrc)

	 do 250 i =1, m
	    d_y(i) = solu(i+n)
  250    continue
	 dxdz = 0.d0
	 do 260 j =1, n
	    d_x(j) = solu(j)
	    d_z(j) = ( r_mu(j) - z(j)*d_x(j) ) / x(j)
	    dxdz   = dxdz + d_x(j)*d_z(j)
  260    continue
    
c      --check accuracy for the direction
	 if (icheck.gt.0) then
	    if (idir.le.1) then
	 call PSMFV(m,ia,ja,an,d_x,xr)
	 call PFVSM(m,n,ia,ja,an,d_y,xc)

	 normdx = 0.d0
	 normdy = 0.d0
	 xyz = 0.d0
	 do 10 i = 1,m
	    xyz = xyz + (xr(i)- r(i+n))**2
	    normdy = normdy + d_y(i)*d_y(i)
   10    continue

c        need H d_x too (** H d_x uses xrc )
	 call PSSMV(n,ih,jh,hn,d_x,xrc)

	 do 20 j = 1,n
	    xyz = xyz + (xc(j)-xrc(j)-(z(j)/x(j))*d_x(j) -r(j))**2
	    normdx = normdx + d_x(j)*d_x(j)
   20    continue
	 xyz = sqrt(xyz)
	 normdx = sqrt(normdx)
	 normdy = sqrt(normdy)
	 if (xyz/(1.+normdx+normdy) .gt. 1.d-10) then
	    write(*,5010) 'direction acc  ', xyz/(1.+normdx+normdy)
	 endif
	    endif
	 endif
c      --check ends
	 
c-3-     determine step length
	 alf_x = big
	 alf_z = big
	 do 310 j =1, n
	    if (d_x(j) .lt. 0.d0) then
	       xyz = - x(j) / d_x(j)
	       alf_x = min(alf_x, xyz)
	    endif
	    if (d_z(j) .lt. 0.d0) then
	       xyz = - z(j) / d_z(j)
	       alf_z = min(alf_z, xyz)
	    endif
  310    continue

c   a.   Ratio
	 alpha_R = min(alf_x, alf_z)
	 if(iprint.gt.1) then
	 write(*,5010) 'alpha_R, x, z  ',alpha_R, alf_x, alf_z
	 endif

c   b.   Line search
c        for x^T z
c        (x + alpha d_x)^T (z + alpha d_z)
c        = x^T z + alpha (x^T d_z + z^T d_x) + alpha^2 (d_x)^T d_z
c        = x^T z ( 1 + alpha*gamma - alpha ) + alpha^2 (d_x)^T d_z
c        = x^T z ( 1 - alpha*(1-gamma) ) + alpha^2 (d_x)^T d_z
c        = (1 -(1-gamma) * alpha + ( d_x^T d_z / x^T z ) alpha^2) x^T z

c        note eta, zeta can be negative and gamma can be greater than 1.
c             ---  ----        --------     -----        --------------
	 xyz = dxdz / comp
	 if (xyz .le. 0.d0) then
c           alpha can be as large as possible
c           but usually increase the N(sigma, beta) and alpha_R is small
	    alpha_L = big
	 else 
c           optimal one
c           alpha_L = 0.5 * (1.d0 -gamma) / xyz
c           not increase
	    alpha_L = (1.d0 -gamma) / xyz
	 endif
	 if(iprint.gt.1) then
	 write(*,5010) 'alpha_L, dxdz  ',alpha_L, dxdz, xyz, comp
	 endif

c   c.   Neighborhood
	 alpha_N = big
	 if(iprint.gt.1) then
	 write(*,5010) 'alpha_N        ',alpha_N
	 endif

	 alpha = min(alpha_R, alpha_L)
	 alpha = min(alpha, alpha_N)

       if(idir .eq. 1) then
	 mu_a = 0.d0
	 do 311 j =1, n
	    mu_a = mu_a + (x(j)+alpha*d_x(j))*(z(j)+alpha*d_z(j))
  311    continue
	 mu_a = mu_a / real(n)
	 if(iprint.gt.1) then
	 write(*,5010) 'mu_a           ',mu_a, mu
	 endif

	 if (.false.) then
	    call linerD(m,n,ia,ja,an,ih,jh,hn,x,y,z,d_x,d_y,d_z,
     1               xc,xr,zeta, c0,c, alpha, normrD_a,reset)
	    write(*,5010) 'normrD_a       ',normrD_a, normrD, 1.-alpha

	    if (alpha.gt.0.999995 .and.
     1          normrD_a/normrD .lt. 0.00001 .and.
     2          mu_a /mu .lt. 0.00001) then
		write(*,5010) 'xxx take affine', alpha
		go to 889
	    endif
	    xyz = alpha*5.d-1
	    call linerD(m,n,ia,ja,an,ih,jh,hn,x,y,z,d_x,d_y,d_z,
     1               xc,xr,zeta, c0,c, xyz, normrD_ah,reset)

	    write(*,5010) 'normrD_a 0.5   ',normrD_ah,normrD,1.-xyz
	 endif
	 idir = 2
	 go to 888
       endif

c   -----predictor-corrector ends --------------------------------
	
	 xyz    = 1.d0/eta
	 alpha  = min(alpha*frac,xyz)
	 alphaP = min(alf_x*frac,xyz)
	 alphaD = min(alf_z*frac,xyz)

c        line search
	 if (.false. ) then
	    k = 0
	    call linerD(m,n,ia,ja,an,ih,jh,hn,x,y,z,d_x,d_y,d_z,
     1               xc,xr,zeta, c0,c, alpha, normrD_a,reset)
	       write(*,5010) 'normrD_a       ',normrD_a
	    xyz = alpha*5.d-1
	    if (normrD_a .le. normrD*(1.-xyz)) go to 892

  891       continue
	    k = k + 1
	    call linerD(m,n,ia,ja,an,ih,jh,hn,x,y,z,d_x,d_y,d_z,
     1               xc,xr,zeta, c0,c, xyz, normrD_ah,reset)
c             write(*,5010) 'normrD_a,  *0.5',normrD_a,normrD_ah, normrD

	    xyz = xyz*5.d-1
	    if (normrD_ah .gt. normrD*(1.-xyz) .and. k.lt.2) then
	       go to 891
	    endif
	       write(*,5010) 'normrD_ah , k  ',normrD_ah, k
	    if (normrD_ah .lt. normrD_a) then
	       write(*,5010) 'normrD_a > *k  ',normrD_a,normrD_ah, k
	       alpha = xyz*2.d0
	    endif
  892       continue
	 endif

c        final step length
	 if(iprint.gt.1) then
	 write(*,5010) 'alpha          ',alpha
	 endif

  889    continue
c-4-     update solution
	 do 410 i =1, m
c           y(i) = y(i) + alphaD * d_y(i)
	    y(i) = y(i) + alpha * d_y(i)
  410    continue
	 do 420 j =1, n
c           x(j) = x(j) + alphaP * d_x(j)
c           z(j) = z(j) + alphaD * d_z(j)
	    x(j) = x(j) + alpha * d_x(j)
	    z(j) = z(j) + alpha * d_z(j)
  420    continue

  500 continue
c     end of main loop

      istop = 3

  900 call myexit(istop)
      write(*,2000) obj+obj_P, obj+obj_D, infrP, infrD
      if (iprint.gt.0) then
     	write(*,*) 
      	write(*,*) '           Primal and Dual Solutions                '
      	write(*,*) ' -------------------------------------------------- '
 	write(*,5100) 'j', 'x(j)', 'z(j)', 'r_D(j)'
	write(*,5101) (i, x(i), z(i), r_D(i), i=1,n)
	write(*,5100) 'i', 'y(i)', 'r_P(i)'
	write(*,5102) (i, y(i), r_P(i), i=1,m)
      endif
 5200 format(/1x,' The LCCP obj value (from direct comp)    ',
     2        1x,1pe21.14
     1       /1x,' The LCCP infeasibility                   ',
     2        1x,1pe10.3)
 5100          format(/1x, a4, 1x,a18, 1x,a18, 1x,a18)
 5101          format(1x, i4, 1x,e18.12, 1x,e18.12, 1x,e18.12)
 5102          format(1x, i4, 1x,e18.12, 1x,e18.12)
      return

 2000 FORMAT( /'          Primal obj. value    =', 1PE21.14
     1        /'          Dual   obj. value    =', 1PE21.14
     1        /'          Primal feas. residual=', 1PE10.3
     1        /'          Dual   feas. residual=', 1PE10.3)
 4010 FORMAT( /'  --iter =',I4, 2X,'Complementarity gap  =',1PE10.3
     1        /'          ',4X, 2X,'Primal obj. value    =',1PE21.14
     1        /'          ',4X, 2X,'Dual   obj. value    =',1PE21.14
     1        /'          ',4X, 2X,'Primal feas. residual=',1PE10.3
     1        /'          ',4X, 2X,'Dual   feas. residual=',1PE10.3)
 4030 FORMAT( /'    gapm=',1PE10.3,2X,'tau=',1PE9.2
     1                            ,2X,'kappa=',1PE9.2
     1                            ,2X,'sigma=',1PE10.3
     2        /'    gap =',1PE10.3,2X,4X,11X,6X,11X,'msigma=',1PE10.3)
 4050 FORMAT( /'    gapm=',1PE10.3,2X,'tau=',1PE9.2
     1                            ,2X,'kappa=',1PE9.2
     1                            ,2X,'sigma=',1PE10.3
     2        /'         ',10X,    2X,4X,11X,6X,11X,'msigma=',1PE10.3)
 5010 FORMAT(1X,'## ',A16,5(1PE8.1,1X))

      end

      subroutine linerD(m,n,ia,ja,an,ih,jh,hn,x,y,z,d_x,d_y,d_z,
     1                  xc,xr,zeta, c0,c, alpha, normrD_a, reset)

      integer m,n, ia(1),ja(1), ih(1),jh(1)
      real*8  an(1),hn(1), x(1),y(1),z(1), d_x(1),d_y(1),d_z(1)
      real*8  xc(1),xr(1), c0(1),c(1)
      real*8  alpha, zeta, normrD_a
      real*8  xyz, zj

c     purpose:  calculate the residual if step is taken according
c               to length alpha.
c     input:
c               m,n
c               ia,ja,an
c               ih,jh,hn
c               x,y,z
c               d_x,d_y,d_z
c               c
c               zeta, alpha
c               reset
c     output:
c               normrD_a
c
c     working space:
c               xc,xr,c0
c
c     notice:   if reset is true the slack of dual is reset.

      logical reset

c +++       H d_x has been calculated in check accuracy(** H d_x uses xrc )
c +++       call PSSMV(n,ih,jh,hn,d_x, r_D_a)

	    do j = 1, n
	       xc(j) = x(j)+alpha*d_x(j)
	    end do
	    call grad(n,c0,ih,jh,hn,xc)
	    do i = 1, m
	       xr(i) = y(i)+alpha*d_y(i)
	    end do
	    call PFVSM(m,n,ia,ja,an,xr, xc)

c +++       xyz = 1.d0 - alpha*zeta

c +++       normrD_ao = 0.d0
	    normrD_a = 0.d0
	    do j = 1, n
c              -- normrD_ao is dependent to the accuracy of the direction
c +++          zj = c0(j) - c(j) - alpha*xrc(j) + xyz*r_D(j)
c +++          normrD_ao = normrD_ao + abs(zj)
c +++          r_D_a(j) = c0(j) - c(j) - alpha*r_D_a(j) + xyz*r_D(j)

	     if(.not.reset) then
	       zj = c0(j) - xc(j) - (z(j) + alpha*d_z(j))
	     else
	       xyz    = c0(j) - xc(j)
	       zj     = z(j) + alpha*d_z(j)
	       if (xyz.gt.0.d0) then
		  xyz    = max(zj*1.d-2, xyz)
		  zj     = min(zj*1.d2, xyz)
	       elseif (xyz.lt.0.d0) then
	       endif
	       zj = c0(j) - xc(j) - zj
	     endif
	       normrD_a = normrD_a + abs(zj)
	    end do
      return
      end
c     Initial point
c
      subroutine init(m,n,x,y,z)

      real*8  x(1),y(1),z(1)
      real*8  xyz

      xyz = 1.d0
      do 10 i =1, n
	 x(i) = xyz
	 z(i) = xyz
   10 continue

      do 20 i =1, m
	 y(i) = 0.d0
   20 continue

      return
      end
c     file  set_m fortran
c
      subroutine set_M(m,n,iat,jat,ant, iua,iu,iju,ju,dn,un, x)

c     Purpose     :  set the reduced KKT matrix M (static part)

      integer iat(1),jat(1), iua(1),iu(1),iju(1),ju(1)
      real*8  ant(1), dn(1),un(1)
      real*8  x(1)

      integer mpn,m,n,km,k,i,j, mu

c     input
c             m,n
c             iat,jat,ant
c             iua,iu,iju,ju
c     output
c             dn,un
c     working
c             mpn,km,k,i,j
c             x               of dimension m

c        fill A(t) in M (note: can do more efficiently, if more than once)
	 do 10 i = 1, m
	    x(i) = 0.d0
   10    continue
	 do 40 j =1, n
c+          sign(j) = -1.
	    do 20 k =iat(j), iat(j+1)-1
	       x(jat(k)) = ant(k)
   20       continue

c           iua points the starting position of A(t)
	    mu = iju(j) - iu(j)
	    do 30 km = iua(j), iu(j+1)-1
	       i = ju(mu+km) - n
	       un(km) = x(i)
	       x(i)   = 0.d0
   30       continue
   40    continue

c        fill zero for rows from n+1 to n+m.
	 mpn = m+n
	 do 50 km =iu(n+1), iu(mpn+1)-1
	    un(km) = 0.d0
   50    continue

c        fill zero for diagonals from n+1 to n+m.
	 do 60 i=n+1, mpn
c+          sign(i)= 1.
	    dn(i) = 0.d0
   60    continue

      return

c     end of set_M()
      end
c     file  xexit fortran
c
      subroutine myexit(istop)

c     Purpose     :  exit

      common  /IT/ iter
      integer iter

      integer istop

	 write(*,*) 
      if      (istop .eq. 0) then
	 write(*,*) ' -- exit : optimality is obtained :) on iter', iter-1
      else if (istop .eq. 1) then
	 write(*,*) ' -- exit : primal is unbounded !     on iter', iter-1
      else if (istop .eq. 2) then
	 write(*,*) ' -- exit : dual   is unbounded !     on iter', iter-1
      else if (istop .eq. 3) then
	 write(*,*) ' -- exit : reach iteration maximum :(  on   ', iter-1 
      else if (istop .eq. 4) then
	 write(*,*) ' -- exit : numerical difficulty :(   on iter', iter-1
      else if (istop .eq. 5) then
	 write(*,*) ' -- exit : infeasibility detected !  on iter', iter-1
      else if (istop .eq. 6) then
	 write(*,*) ' -- exit : insufficient space :(     on iter', 0
	 write(*,*)
	 write(*,*)
     x'          --------------------------------------------'
	 write(*,*)
     x'          contact COL of UI for advanced version      '
	 write(*,*)
     x'          Yinyu Ye (yinyu-ye@uiowa.edu)               '
	 write(*,*)
     x'          --------------------------------------------'
      else if (istop .eq. 7) then
	 write(*,*) ' -- exit : error in data file       on iter', 0
      else
	 write(*,*) 'XXX istop wrong'  
      end if

      return
c     end of myexit
      end

