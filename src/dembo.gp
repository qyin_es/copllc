(# of cons,# of vars,# of non0s in A,# of non0s in the up-half H)
 3 4 8 5
obj constant coefficients
  1.
  1.
  .25
  1.
right hand vector (b)
  1.
  0.
  0.
# of non0s in each row of A (from top and down)
 2
 3
 3
column indices and numerical values of non0s in A (row by row)
 1    1.
 2    1.
 1    1.
 2   -1.
 3    .5
 1    1.
 2   -1.
 4    1.
# of non0s in each row of the up-half H
 1
 1
 2
 1
column indices and numerical values of non0s in the up-half H
 1   1.
 2   1.
 3   1.
 4   1.
 4   1.
END
