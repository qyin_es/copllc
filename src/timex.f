C
C      etime
C
C       FUNCTION ETIME(USR,SYS)
       FUNCTION ETIME(USR)
       REAL*4   USR(2), ETIME
       external times
       integer  times
       integer  timtir
       common   /timcom/ timfir
       integer  tres, timarr(4)
       tres = times(timarr)
       timfir = timarr(1)
       hz = 100.0
       ETIME  = real(timarr(1))/hz
       RETURN
       END
C
C      dtime
C
C       FUNCTION DTIME(USR,SYS)
       FUNCTION DTIME(USR)
       REAL*4   USR(2), DTIME
       external times
       integer  times
       integer  timtir
       common   /timcom/ timfir
       integer  tres, timarr(4)
       tres = times(timarr)
       hz = 100.0
       DTIME = real(timarr(1) - timfir)/hz
       timfir = timarr(1)
       RETURN
       END
