c     machine-dependent routines
c     --------------------------
c         pull6c, psmfv, pfvsm, pssmfv.f
c         pc, workstation, vector and parallel machine versions
c
c
C     File  copyi fortran
C
      SUBROUTINE COPYI(N,A1,A2)
      INTEGER A1(N),A2(N)
C
C     FORWARD COPY of Two Integral Vectors: A2=A1
C
      DO 10 J = 1, N
   10 A2(J) = A1(J)
      RETURN
C     END OF COPYI
      END

      SUBROUTINE COPYIB(N,A1,A2)
      INTEGER A1(N),A2(N)
C
C     BACKWARD COPY Two Integral Vectors: A2=A1
C
      DO 10 J = N, 1, -1
   10 A2(J) = A1(J)
      RETURN
C     END OF COPYI
      END
C     File  copyr fortran
C
C
      SUBROUTINE COPYR(N,A1,A2)
      REAL*8 A1(N),A2(N)
C
C     FORWARD COPY of Two Real Vectors: A2=A1
C
      DO 10 J = 1, N
   10 A2(J) = A1(J)
      RETURN
C     END OF COPYR
      END

      SUBROUTINE COPYRB(N,A1,A2)
      REAL*8 A1(N),A2(N)
C
C     BACKWARD COPY of Two Real Vectors: A2=A1
C
      DO 10 J = N, 1, -1
   10 A2(J) = A1(J)
      RETURN
C     END OF COPYR
      END
C     File  dpfv fortran
C
      FUNCTION DPFV(N,X,Y)
C
C     Purpose     :  Dot product of two full real vectors.
C                 :  DPFV=X^TY
C     Input:  X            given full vectors of order N
C     Output: DPFV()       dot product of X and Y
C
C     REAL    X(N), Y(N), DPFV
      REAL*8  X(N), Y(N), DPFV
      DPFV = 0.D0
      DO 10 I = 1, N
         DPFV = DPFV + X(I)*Y(I)
  10  CONTINUE
      RETURN
      END
C     File  pfvsm fortran
C
      SUBROUTINE PFVSM(N,M,IA,JA,AN,B,C)
C
C     Purpose     :  product of a full row vector and a sparse matrix
C                 :  C=B^TA
C     Input:  IA, JA, AN   given matrix in RR(C)U.
C             B            given full vector.  
C             N            number of rows of the matrix,
C                          and the order of B.
C             M            number of columns of matrix.
C     Output: C            resulting vector, of order M.
c     INTEGER IA(N+1), JA(NON)
c     REAL*8  AN(NON), B(N), C(M), Z 
      INTEGER IA(1), JA(1)
      REAL*8  AN(1), B(1), C(1), Z 

      REAL*4  tfac,taat,tllt,tax,taty, ETIME,USR(2), time0
      INTEGER ifac,iaat,illt,iax,iaty
      COMMON  /timest/tfac,taat,tllt,tax,taty, ifac,iaat,illt,iax,iaty

         time0 = ETIME(USR)

      DO 10 I = 1, M
         C(I)= 0.D0
  10  CONTINUE
      DO 30 I = 1, N
         IAA = IA(I)
         IAB = IA(I+1) - 1
c        IF(IAB.LT.IAA) GO TO 30
         Z = B(I)
         DO 20 K = IAA, IAB
            C(JA(K)) = C(JA(K)) + AN(K)*Z
  20     CONTINUE
  30  CONTINUE

         taty  = taty + ETIME(USR) - time0
         iaty  = iaty + 1

      RETURN
      END
C     File  psmfv fortran
C
      SUBROUTINE PSMFV(N,IA,JA,AN,B,C)
C
C     Purpose     :  product of a sparse matrix and a full column vector
C                 : C=AB
C     Input:  IA, JA, AN   given matrix in RR(C)U.
C             B            given full vector.
C             N            number of rows of the matrix.
C             M            number of columns of matrix,
C                          and the order of B.
C     Output: C            resulting vector, of order N.
C     INTEGER IA(N+1), JA(NON)
C     REAL*8  AN(NON), B(M), C(N), Z
      INTEGER IA(1), JA(1)
      REAL*8  AN(1), B(1), C(1), Z

      REAL*4  tfac,taat,tllt,tax,taty, ETIME,USR(2), time0
      INTEGER ifac,iaat,illt,iax,iaty
      COMMON  /timest/tfac,taat,tllt,tax,taty, ifac,iaat,illt,iax,iaty

         time0 = ETIME(USR)

      DO 20 I = 1, N
         Z = 0.D0
         IAA = IA(I)
         IAB = IA(I+1) - 1
c        IF(IAB.LT.IAA) GO TO 20
         DO 10 K = IAA, IAB
            Z = Z + AN(K)*B(JA(K))
  10     CONTINUE
         C(I) = Z
  20  CONTINUE

         tax  = tax + ETIME(USR) - time0
         iax  = iax + 1

      RETURN
      END
C     File  pssmfv fortran
C
      SUBROUTINE PSSMV(N,IA,JA,AN,B,C)
C
C     Purpose     :  product of a symetric sparse matrix and a full column vector
C                 : C=AB
C     Input:  IA, JA, AN   given matrix in RR(U)O.
C             B            given full vector.
C             N            dimension size of the matrix.
C     Output: C            resulting vector, of order N.
C     INTEGER IA(N+1), JA(NON)
C     REAL*8  AN(NON), B(N), C(N), Z
      INTEGER IA(1), JA(1)
      REAL*8  AN(1), B(1), C(1), Z, BI

      REAL*4  tfac,taat,tllt,tax,taty, ETIME,USR(2), time0
      INTEGER ifac,iaat,illt,iax,iaty
      COMMON  /timest/tfac,taat,tllt,tax,taty, ifac,iaat,illt,iax,iaty

         time0 = ETIME(USR)

      DO 10 I = 1, N
         C(I)= 0.D0
  10  CONTINUE
      DO 30 I = 1, N

         IAA = IA(I)
         IAB = IA(I+1) - 1
         if (IAB.GE.IAA) then
            BI  = B(I)

c           diagonal
	    if ( JA(IAA). eq. I) then
c              A is ordered. Otherwise diagonal may not be the first entry.
c              At least, diagonal shoulb be the first entry, if any.
	       Z   = AN(IAA) * BI
	       IAA = IAA + 1
            else
	       Z   = 0.d0
            endif

            DO 20 K = IAA, IAB
            Z = Z + AN(K)*B(JA(K))
	    C(JA(K)) = C(JA(K)) + AN(K)*BI
  20        CONTINUE

            C(I) = C(I) + Z
         endif
  30  CONTINUE

         taat = taat+ ETIME(USR) - time0
         iaat = iaat+ 1

      RETURN
      END
C     File  pull6 fortran
C
      SUBROUTINE PULL(N,DN,IJU,JU,IU,UN, IL,JL, X, TOLPIV,FLAG, SIGN)
C
C     Purpose     :  Pulling Cholesky factorization
C
C     Input : N, DN, IJU,JU,IU, UN,TOLVIP
C     Output: DN,UN, FLAG
C
      IMPLICIT REAL*8(A-G,O-Z)
      INTEGER IJU(1),JU(1),IU(1), IL(1),JL(1), FLAG
      REAL*8  DN(1), UN(1), X(1)
      REAL*8  A,DK 

c+++  indefinite
      REAL*8  SIGN(1)

      COMMON /PIVOT/NJU,NZP
      INTEGER NJU, NZP
      REAL*8  TOLPIV
      REAL*8  pivmin 

      COMMON /ERROR/IERR

      COMMON  /timest/tfac,taat,tllt,tax,taty, ifac,iaat,illt,iax,iaty
      REAL*4  tfac,taat,tllt,tax,taty, ETIME,USR(2), time0
      INTEGER ifac,iaat,illt,iax,iaty

         time0 = ETIME(USR)
Cpivot
      NZP = 0

      FLAG = 0
      pivmin = 1.D+0

C     Initialize JL
      DO 10 K = 1,N
         JL(K) = 0
   10 CONTINUE

C     For each row do
      DO 200 K=1,N
C        Initialize DK and X
         JMIN = IU(K)
         JMAX = IU(K+1) - 1
         IF (JMIN.LE.JMAX) THEN
            MU = IJU(K) -IU(K)
            DO 20 J=JMIN,JMAX
               X(JU(MU+J)) = UN(J)
   20       CONTINUE
	 ENDIF
         DK = DN(K)

c        pull all columns in link JL
         NXTI = JL(K)
C        while ( NXTI .NE. 0 ) do
  100    I = NXTI
         IF (I.NE.0) THEN
            NXTI = JL(I)
c           collect all contributions to X
c           adjust IL and JL

c           IS...I...IE  supernode

            A = - UN(IL(I))*DN(I)
            DK = DK + A*UN(IL(I))
            UN(IL(I)) = A

            JMIN = IL(I) + 1
            JMAX = IU(I+1) - 1
            IF (JMIN.LE.JMAX) THEN
               MU = IJU(I) - IU(I)
               DO 30 J=JMIN,JMAX
                  X(JU(MU+J)) = X(JU(MU+J)) + A*UN(J)
   30          CONTINUE
               IL(I) = JMIN
               J = JU(MU+JMIN)
               JL(I) = JL(J)
               JL(J) = I
            ENDIF
            GO TO 100
         ENDIF

C        Set DN(K) and copy X into Kth row of UN
         JMIN = IU(K)
         JMAX = IU(K+1) - 1
c+++     indefinite --- SIGN(K)
	 if (SIGN(K)*DK.GE.TOLPIV) then
c+++     IF (DK.GE.TOLPIV) THEN
	    DN(K) = 1.D0 / DK
	    IF (JMIN.GT.JMAX) GO TO 200
	    MU = IJU(K) - JMIN
	    DO 150 J=JMIN,JMAX
               UN(J) = X(JU(MU+J))
  150       CONTINUE
         elseif(SIGN(K).lt.0.d0 .and. DK.lt. 0.d0) then
	    write(*,*) " xx- pivot error ",K, SIGN(K), DK
	    DN(K) = 1.D0 / DK
	    IF (JMIN.GT.JMAX) GO TO 200
	    MU = IJU(K) - JMIN
	    DO 152 J=JMIN,JMAX
	       UN(J) = X(JU(MU+J))
  152       CONTINUE
	 ELSE
            write(*,*) " xxx pivot error ",K, SIGN(K), DK
            NZP = NZP + 1
	    JU(NZP+NJU) = K
            DN(K) = SIGN(K)*1.D0
c+++        DN(K) = 1.D0
            IF (JMIN.GT.JMAX) GO TO 200
            MU = IJU(K) - JMIN
            DO 151 J=JMIN,JMAX
               UN(J) = 0.D0
  151       CONTINUE
	    pivmin= min(pivmin,SIGN(K)*DK)
c+++        pivmin= min(pivmin,DK)
	    FLAG  = FLAG + 1
         ENDIF
         IL(K) = JMIN
         I     = JU(MU+JMIN)
         JL(K) = JL(I)
         JL(I) = K

  200 CONTINUE

      if (FLAG .gt. 0) then
      WRITE(IERR,*)'XXX too small or negative pivot ',FLAG,pivmin
      endif

         tfac  = tfac + ETIME(USR) - time0
         ifac  = ifac + 1

      RETURN
      END
C     File  sns fortran
C
      SUBROUTINE SNS(N, P, D, IJU, JU, IU, U, Z, B, TMP)
C
C     Purpose     :  Forward and backward substitution 
C
C     Input:  N, P, D, IJU, JU, U, B
C     Output: Z
C     Working space: TMP - vector of size N for solving  Ut Dy = b.
C
      IMPLICIT REAL*8(A-G,O-Z)
      INTEGER P(1), IJU(1), JU(1), IU(1)
      REAL*8 D(1),U(1), Z(1), B(1), TMP(1)
c     REAL TMPK,SUM 

      INTEGER NJU, NZP
      COMMON /PIVOT/NJU,NZP

      REAL*4  tfac,taat,tllt,tax,taty, ETIME,USR(2), time0
      INTEGER ifac,iaat,illt,iax,iaty
      COMMON  /timest/tfac,taat,tllt,tax,taty, ifac,iaat,illt,iax,iaty

         time0 = ETIME(USR)

C     Initialize TMP to the reordered B
      DO 1 K=1,N
         TMP(K) = B(P(K))
  1   CONTINUE

C     Solve Ut Dy = b by forward substitution
c     pushing
      DO 3 K=1,N
         TMPK = TMP(K)
         JMIN = IU(K)
         JMAX = IU(K+1) - 1
c hot!   if super node is considered, then can save index
c        calculation and do loop unrolling here. different
c        from factorization phase, cache size limitation does
c        not apply to substitution phase
         IF (JMIN.LE.JMAX) THEN
            MU = IJU(K) - JMIN
            DO 2 J=JMIN,JMAX
               JJ = JU(MU+J)
               TMP(JJ) = TMP(JJ) + U(J)*TMPK
  2         CONTINUE
	 ENDIF
         TMP(K) = TMPK * D(K)
  3   CONTINUE

C     make modification if zero or negative pivot occurs
      IF (NZP.GT.0) THEN
         DO 11 K =1, NZP
C           J = IZP(K)
	    J = JU(K+NJU)
            TMP(J) = 0.D0
  11     CONTINUE
      ENDIF

C     Solve Ux = y by back substitution
c     pulling
      K = N
      DO 6 I=1,N
         SUM  = TMP(K)
         JMIN = IU(K)
         JMAX = IU(K+1) - 1
c hot!   can do roop unrolling here
         IF (JMIN.LE.JMAX) THEN
            MU = IJU(K) - JMIN
            DO 4 J=JMIN,JMAX
               SUM = SUM + U(J)*TMP(JU(MU+J))
   4        CONTINUE
	 ENDIF
         TMP(K) = SUM
         Z(P(K))= SUM
         K = K-1
   6  CONTINUE

         tllt  = tllt + ETIME(USR) - time0
         illt  = illt + 1

      RETURN
      END
C     File  ssf fortran
C
      SUBROUTINE SSF(MUA,N,P,IP,IA,JA,IJU,JU,IUA,IU,
     1               JUMAX,Q,JL,FLAG,LENJU,LENU)
C
C     Purpose     :  Symbolic factorization of a sparse symmetric matrix
C
C     Input:   N, P,IP, IA,JA, JUMAX
C     Output:  IJU,JU,IU,FLAG
C
      INTEGER P(1),IP(1),IA(1),JA(1),IJU(1),JU(1),IUA(1),IU(1),
     *        Q(1),JL(1),FLAG, VJ,QM
    
      COMMON /ERROR/IERR

C     Initialize
      JUMIN = 1
      JUPTR = 0
      IU(1) = 1
      DO 1 K=1,N
         JL(K) = 0
 1    CONTINUE

C     For each row do
      DO 15 K=1,N
C        Initialize Q to structure of Kth row above diagonal
         LUK = 0
         Q(K) = N+1
         JMIN = IA(P(K))
         JMAX = IA(P(K)+1)-1
         IF (JMIN.GT.JMAX) GO TO 101
         DO 3 J=JMIN,JMAX
            VJ = IP(JA(J))
            IF (VJ.GT.K) THEN
               QM = K
   2           M = QM
               QM = Q(M)
               IF (QM.LT.VJ) THEN
		  GO TO 2
               ELSEIF (QM.GT.VJ) THEN
                  LUK = LUK+1
                  Q(M) = VJ
                  Q(VJ) = QM
               ELSE
                  GO TO 102
               ENDIF
            ENDIF
   3     CONTINUE

C        Compute fillin for Q by
         LMAX = 0
         IJU(K) = JUPTR
         I = K

C        Linking through JL and
   4     I = JL(I)
         IF (I.NE.0) THEN
             LUI = IU(I+1) - (IU(I)+1)
             JMIN = IJU(I) + 1
             JMAX = IJU(I) + LUI
             IF (LUI.GT.LMAX) THEN
                LMAX = LUI
                IJU(K) = JMIN
             ENDIF
             QM = K
C            Merging each row with Q
             DO 7 J=JMIN,JMAX
                VJ = JU(J)
   6            M = QM
                QM = Q(M)
                IF (QM.LT.VJ) THEN
		   GO TO 6
                ELSEIF (QM.GT.VJ) THEN 
                   LUK = LUK+1
                   Q(M) = VJ
                   Q(VJ) = QM
                   QM = VJ
                ENDIF
   7         CONTINUE

             GO TO 4
          ENDIF

C         Check if row duplicates another.  If not
          IF (LUK.NE.LMAX) THEN
C            see if tail of K-1st row matches head of Kth
             IF (JUMIN.GT.JUPTR) GO TO 12
             I = Q(K)
             DO 9 JMIN=JUMIN,JUPTR
                IF (JU(JMIN)-I) 9, 10, 12
   9         CONTINUE
             GO TO 12
  10         IJU(K) = JMIN
             DO 11 J=JMIN,JUPTR
                IF (JU(J).NE.I) GO TO 12
                I = Q(I)
                IF (I.GT.N) GO TO 14
  11         CONTINUE
             JUPTR = JMIN - 1

C            Set Kth row of U to Q
  12         JUMIN = JUPTR + 1
             JUPTR = JUPTR + LUK
             IF (JUPTR.GT.JUMAX) GO TO 106
             I = K
             DO 13 J=JUMIN,JUPTR
                I     = Q(I)
                JU(J) = I
  13         CONTINUE
             IJU(K) = JUMIN
         ENDIF

C        If more than one element in row ,adjust JL
  14     IF (LUK.GT.1) THEN
            I     = JU(IJU(K))
            JL(K) = JL(I)
            JL(I) = K
         ENDIF
         IU(K+1) = IU(K) + LUK

  15  CONTINUE

      FLAG = 0
      LENU   = IU(N+1) - 1
      LENJU  = IJU(N)

c+    need to consider P and IP later on
      do 30 I =1, N 
	 KMIN   = IU(I)
	 KMAX   = IU(I+1) - 1
	 IUA(I) = KMAX + 1
	 MU     = IJU(I) - KMIN
	 do 20 K =KMIN, KMAX
	    if (JU(K+MU) .gt. MUA) then
	       IUA(I) = K
	       go to 30
            endif
  20     continue
  30  continue

      RETURN

C     ERROR MESSAGE
 101     FLAG = N + P(K)
         WRITE(IERR,*) 'IN SSF : NULL ROW IN A'
         RETURN
 102     FLAG = 2*N + P(K)
         WRITE(IERR,*) 'IN SSF : Duplicate Entry in A'
         RETURN
 106     FLAG = 6*N + K
         WRITE(IERR,*) 'IN SSF : Insufficient Storage for JU'
         RETURN
      END
C     File  tgsm fortran
C
      SUBROUTINE TGSM(N,M,IA,JA,AN,IAT,JAT,ANT)
C
C     Purpose     :  transposition of a general sparse matrix
C
C     Input:  IA, JA, AN   given matrix in RR(C)U.
C             N            number of rows of the matrix.
C	      M            number of columns of the matrix.
C     Output: IAT,JAT,ANT  transposed matrix in RR(C)O.
C
C     INTEGER IA(N+1), JA(NON), IAT(M+1), JAT(NON)
C     REAL*8  AN(NON), ANT(NON)
      INTEGER IA(1), JA(1), IAT(1), JAT(1)
      REAL*8  AN(1), ANT(1)
      MH = M + 1
      NH = N + 1
      DO 10 I = 2, MH
         IAT(I) = 0
   10 CONTINUE
      IAB = IA(NH) -1
      DO 20 I = 1, IAB
         J = JA(I) + 2
         IF(J.LE.MH) IAT(J) = IAT(J) + 1
   20 CONTINUE
      IAT(1) = 1
      IAT(2) = 1
      IF (M.NE.1) THEN
         DO 30 I = 3, MH
            IAT(I) = IAT(I) + IAT(I-1)
   30    CONTINUE
      ENDIF
      DO 60 I = 1, N
         IAA = IA(I)
         IAB = IA(I+1) - 1
         IF (IAB.GE.IAA) THEN
            DO 50 JP = IAA, IAB
               J = JA(JP) + 1
               K = IAT(J)
               JAT(K) = I 
               ANT(K) = AN(JP)
               IAT(J) = K + 1
   50       CONTINUE
	 ENDIF
   60 CONTINUE
      RETURN
      END

