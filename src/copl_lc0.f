c     file  copl_lc0 fortran
c
      subroutine copl_lc0(core, lensp)

c     Purpose     :  Initialize the system to solve linearly constrained
c	             convex programming
c     Date        :  May, 97
c     By          :  Yinyu Ye
c
c     Set the main stack
  !ms$if .not. defined(LINKDIRECT)
  !ms$attributes dllexport :: copl_lc0
  !ms$endif
      real*8  core(1)
      integer lensp

      integer ratio, lsp
      common  /SPA/ ratio, lsp
      integer STDIN, STDOUT
      common  /IO/ STDIN, STDOUT
      COMMON  /MACHINCE/ IMACH,ICACHE
      COMMON  /ERROR/IERR
      parameter (MaxColumns=20000, MaxRows=5000)
      common /LCCP/cc
      real*8  cc(MaxColumns)
      
      integer m,n, ia,ja,an, b,c, ih,jh,hn, x,y,z, ihead, istop
      real*8  obj

      real*4  ETIME, USR(2), time0,time1,time2,time3

ccc   time statistic for factorization,AA(t),substitution,Ax,A(t)y
      REAL*4  tfac,taat,tllt,tax,taty
      INTEGER ifac,iaat,illt,iax,iaty
      COMMON  /timest/tfac,taat,tllt,tax,taty, ifac,iaat,illt,iax,iaty
      DATA    tfac,taat,tllt,tax,taty, ifac,iaat,illt,iax,iaty
     1        /0.,0.,0.,0.,0., 0,0,0,0,0/

      time0 = ETIME(USR)

      ispace = 0
      icheck = 0

      ratio = 2
      STDIN = 5
      STDOUT= 6
      ICACHE= 256
      IERR  = STDOUT
      lsp   = lensp
      write(*,*) 
      write(*,*) ' ==================================================='
      write(*,*) ' COPL_LC -- Linearly Constrained Convex Programming '
      write(*,*) '            by  COL, U of Iowa                      '
      write(*,*) ' ==================================================='
      write(*,*) 
      write(*,*)
 
         if (ispace.gt.0) then
            write(*,*) ' space for drdata', lsp
         endif

c     ==========
c     input data
c     ==========

      call drdata(m,n, ia,ja,an, b,c, ih,jh,hn, obj, core(1), iflag)
      time1 = ETIME(USR)
      if (iflag .ne. 0) then
	 istop = iflag
         go to 900
      endif

c     =================
c     solve the problem
c     =================

      ihead = lensp - lsp + 1
      x     = ihead
      y     = x + n
      z     = y + m
      ihead = z + n
      lsp   = lensp - ihead + 1

         if (ispace.gt.0) then
            write(*,*) ' space for drpdpf', lsp
         endif
         if (icheck.gt.0) then
            write(*,*) m,n, ia,ja,an, b,c, ih,jh,hn
            write(*,*) m,n, c,b, ia,ja,an, ih,jh,hn
            write(*,*) lensp, lsp, x,y,z
         endif

      call drpdpf(m,n, core(ia),core(ja),core(an), core(b),core(c),
     1            core(ih),core(jh),core(hn), obj,
     2            core(x),core(y),core(z), core(ihead), istop)

  900 time2 = ETIME(USR)

      write(*,*)
      write(*,*) '         Termination Status and Statistics          '
      write(*,*) ' ---------------------------------------------------'
      write(*,1003)      istop

c     ========
c     all done
c     ========

      time3 = ETIME(USR)
      write(*,     4000) time1-time0,time2-time1,time3-time0
c      write(*,     4000) time1,time2-time1,time3      

      r__1 = tfac / max(1,ifac)
      r__2 = taat / max(1,iaat)
      r__3 = tllt / max(1,illt)
      r__4 = tax  / max(1,iax)
      r__5 = taty / max(1,iaty)

c     write(*,     4001)  tfac,ifac,r__1 ,taat,iaat,r__2,
c    1                    tllt,illt,r__3 ,tax,iax,r__4 ,taty,iaty,r__5

      return

 1003 format(/1x,' Program terminated where termination status  = ',i4/
     x        1x,' 0=optimal, 1=primal unbounded, 2=dual unbounded'/
     x        1x,' 3=iteration maximum, 4=numerical difficulty'/
     x        1x,' 5=primal or dual infeasible, 6=insufficient space'/
     x        1x,' 7=data file error')
 4000 FORMAT(/1x,
     1    '      time for data readin = ',f10.2
     2/1x,'           problem solving = ',f10.2
     3/1x,'     total time in seconds = ',f10.2)

 4001 format(/3x, 'time elapsed for factorization:',f10.4,2x,i4,f10.5,
     1       /3x, 'time elapsed for ADA(t):       ',f10.4,2x,i4,f10.5,
     1       /3x, 'time elapsed for substitution: ',f10.4,2x,i4,f10.5,
     1       /3x, 'time elapsed for Ax:           ',f10.4,2x,i4,f10.5,
     1       /3x, 'time elapsed for A(t)y:        ',f10.4,2x,i4,f10.5)

c     end of copl_lc0()
      end

